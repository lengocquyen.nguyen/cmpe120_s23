#Lets learn Python basics

#Single line comments
"""
Multi line comments
"""

"""
# Topics
1. Print syntax
2. Variables - free format declaration (camelcase/snake case), case sensitive, single/double quotes
3. Datatypes - int, float, str, list, dict, bool
4. Casting
5. Strings
6. Operators
7. if-else condition
8. while loop
9. Functions
"""
